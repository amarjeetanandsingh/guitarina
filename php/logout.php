<?php
// logout function.
function logout(){

		session_start();
		if(	isset($_SESSION['loggedIn']) &&
			$_SESSION['loggedIn'] = "true"){

			$_SESSION['loggedIn'] = "false";
			session_destroy();

			// unset COOKIES.
			setcookie('token', "", 1,"/");
			setcookie('email',"",1,"/");
			setcookie('id',"", 1,"/");

			// redirect to login.php page to login again.
			header("Location: ../login.php");

		}else{
			echo 'you aren\'t logged in.';
			// redirect to login.php page to login again.
			header("Location: ../login.php");

		} // if if you aren't logged in.


} 	// logout function closes.

logout();

?>
