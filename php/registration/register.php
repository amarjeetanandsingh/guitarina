<?php

include_once '../db_connect.php';   // As functions.php is not included

include "../escapeInput.php";
// password_hash() support to php5.4.45 using password_compt.php
include '../../lib/password_compt.php'  ;


/* check connection */
if ($link ->connect_errno) {
    printf("Connect failed: %s\n", $link ->connect_error);
    exit();
}else{

	/* do your stuff once database is connected.*/

// declearing variables for user input to be stored securely.
	$name="";
	$fatherName="";
	$dob="";
	$tutorial ="";
	$email = "";
	$phone ="";
	$password = "";
	$confirmPassword = "";
	$tempPassword = "";
	$tempConfirmPassword = "";
	$flag = TRUE;

	
	// if the submit is set...
	if(isset($_POST['submit']) ){

		
		// Name ....................
		if(preg_match("%^[a-z A-Z]{3,100}$%",stripslashes(trim($_POST['name'])))){
			$name = escapeInput($link,  $_POST['name']);
		}
		else{
			$flag=false;
			die ('<font color="red">Please enter a valid Name</font>');
		}

		// Father Name ..................
		if(preg_match("%^[a-z A-Z]{3,100}$%",stripslashes(trim($_POST['fathername'])))){
			$fatherName = escapeInput($link,  $_POST['fathername']);
		}
		else{
			$flag=false;
			die ('<font color="red">Please enter a valid Father Name</font>');
		}

		// dob ..................
		if(preg_match("%^[0-9]{4}/[01][0-9]/[0-9]{2}$%",stripslashes(trim($_POST['dob'])))){
			$dob = escapeInput($link,  $_POST['dob']);
		}
		else{
			$flag=false;
			die ('<font color="red">Please enter Date of birth in YYYY/MM/DD format.</font>');
		}


		// tutorial ..................
		$rawTutorial = stripslashes(trim($_POST['tutorial']));
		if(	$rawTutorial == "guitar" ||
			$rawTutorial == "synth" ||
			$rawTutorial ==	"vocal" ||
			$rawTutorial == "violin"){
			$tutorial = escapeInput($link,  $_POST['tutorial']);
		}
		else{
			$flag=false;
			die ('<font color="red">Invalid Tutorial type.</font>');
		}

		$email = escapeInput($link, $_POST['email']);
		// email ..................
			// $email = escapeInput($link, $_POST['email']);
		
		/******************
			onBlur AND onSubmit call AJAX to check if email entered is unique or not.
		********************************/


		// phone ..................
		if(preg_match("%^[0-9]{10}$%",stripslashes(trim($_POST['phone'])))){
			$phone = escapeInput($link,  $_POST['phone']);
		}
		else{
			$flag=false;
			die ('<font color="red">Please enter 10 digit valid phone number.</font>');
		}


		// password....................
		$tempPassword = stripslashes($_POST['password']);
		$tempConfirmPassword = stripslashes($_POST['confirmPassword']);
		if(strlen($tempPassword)<6){
			$flag = false;
			die ('<font color="red">Passwrod must be at least 6 characters long.</font>');
		}
		else if(strcasecmp($tempPassword, $tempConfirmPassword) !=0){
			$flag=false;
			die ('<font color="red">Both passwords you entered do not match.</font>');
		}
		else
			$password = escapeInput($link,  $_POST['password']);

	}else if (!$resp->is_valid) {
    // What happens when the CAPTCHA was entered incorrectly
		$flag=false;
    	die ("The reCAPTCHA wasn't entered correctly. Go back and try it again." .
        	 "(reCAPTCHA said: " . $resp->error . ")");
}
	// if all field are valid, flag must be TRUE.
	if($flag == TRUE){
		
		//include ../isEmailRegistered.php to verify if the email already registered.
		include('../email/isEmailRegistered.php');
		$isEmailRegistered=isEmailRegistered($email, $link );

		//if email is not taken.
		if($isEmailRegistered == false){
			// .... inserting this record to guitarina_db->members table........


			// passwordHash.
			$password=password_hash($password, PASSWORD_DEFAULT);
			
			$query = ("	INSERT INTO members (name,father,dob,email,password,phone)	
						VALUES(	'$name','$fatherName','$dob','$email','$password','$phone');");
			
			$result = mysqli_query($link , $query)
				or trigger_error(mysqli_error($link ));
			if(mysqli_affected_rows($link )==1){			
			echo "Thanks for registerig.";

			//mail verification module called if registered successfully.
			include('../email/mail_verif_key.php');
			mail_verif_key($email, $link );
				
			}else{
				echo "You cannot register due to system error.";
			}

		}else{	// if email is not taken.
			die ("<font color='red'>Email already taken. Please enter unique emailID.</font>");
		}

	}
	}