<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Guitarina: Online Tutorials</title>

<link rel="shortcut icon" href="../images/title.png" type="image/png">
<link rel="icon" href="../images/title.ico" >
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no">

	<link href="../css/bootstrap.min.css" rel="stylesheet">
	<link rel="stylesheet" href="../style.css" type="text/css">
	<link href="css/lightbox.css" rel="stylesheet">
	<link href='http://fonts.googleapis.com/css?family=Poppins:400,600,700,500,300' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Roboto:400,900italic,900,700italic,700,400italic,500,500italic,300,100italic,100,300italic' rel='stylesheet' type='text/css'>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

<!-- reCaptcha -->
<script src='https://www.google.com/recaptcha/api.js'></script>

</head>

<body>
<header class="header"> 
  <div class="container"> 
    <div class="row"> 
      <div class="col-md-4 "> 
        <div class="navbar-header">
          <button data-toggle="collapse" data-target="#myNavbar" class="navbar-toggle menu-button" type="button">
          <span class="glyphicon glyphicon-align-justify" aria-hidden="true"></span>
        </button>
          <!-- the brand logo of guitarina.png ************** -->
					    <a class="navbar-brand" href="#">
					        <img height="40px" width="40px" alt="Brand" src="../images/logo4.png">
					      </a>

      					<a class="navbar-brand logo" href="#">Guitarina.com</a>
             
        </div> 
      </div> 
      <div class="col-md-8"> 
        <nav id="myNavbar" role="navigation" class="collapse navbar-collapse"> 
          <ul class="nav navbar-nav navbar-right menu"> 
            <li><a class="page-scroll" href="index.html">Home</a></li> 
            <li><a class="page-scroll" href="videos.php">Tutorials</a></li> 
            <li><a class="page-scroll" href="login.php">Classes</a></li>
			       <li><a class="page-scroll" href="gallery.php">Gallery</a></li>
            <li><a class="page-scroll" href="#section2">About Us</a></li> 
            <li><a class="page-scroll" href="#section4">Contact</a></li> 
          </ul> 
        </nav> 
      </div> 
    </div> 
  </div> 
  </header> 
