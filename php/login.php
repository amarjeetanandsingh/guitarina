<?php


// declearing variables to hold user input.
	$email = "";
	$password_Input = "";
	$password_org = "";
	$salt = "";
	$iteration =0;
	

include_once 'db_connect.php';   // included to connect to database.

include_once('escapeInput.php');

// password_hash() support to php5.4.45 using password_compt.php
include '../lib/password_compt.php'  ;


/* check connection */
if ($link ->connect_errno) {
    printf("Connection to database failed: %s\n", $link ->connect_error);
    exit();
}else{

	// sanitise and validate inputs
		$email = escapeInput($link,  trim($_POST['email']));
		$password_Input = stripslashes($_POST['password']);
		
		
	// retrieve password from database for given email
		$query = "SELECT (password) FROM members WHERE email ='$email'";
		$result = mysqli_query($link , $query)
					or trigger_error(mysqli_error($link ));
		if(!$result){
			die ("<font color='red'>Error occured while retrieving password from database.</font>");
		}else if(mysqli_num_rows($result)==1){
			// if the password is retrieved successfully.
			
			$row = mysqli_fetch_array($result);

			
			if(password_verify($password_Input, $row['password'])){
				// once login is successfull.

				 if(isset($_POST['rememberme']) && $_POST['rememberme'] == true){
					
					echo '<br>remember me true<br>';
					// do stuff to auto_login ...........
					//.......REMEMBER ME..................
					// create unique token to be stored in the database.
					$token = uniqid(mt_rand());

					// create password_hash of $token to be sent to user in cokkies[].
					$token_hash = password_hash($token, PASSWORD_DEFAULT);


					// insert row with $token into AUTH_TOKEN
					$query = "	INSERT INTO auth_token(token,email,status)
								VALUES('$token_hash','$email','unused')";
					$result = mysqli_query($link , $query)
								or trigger_error(mysqli_error($link ));

					if(mysqli_affected_rows($link )==1){
					// if row is inserted successfully... 
						
						// get auto generated id.
						$auth_token_id = mysqli_insert_id($link );
						
						// SET COOKIES.
						setcookie('token', $token, time()+60,"/");
						setcookie('email',$email,time() + 60,"/");
						setcookie('id',$auth_token_id, time()+ 60,"/");

						// set SESSION.

						// *********************
						// SECUTIRY TASK TO COMPLETE...
						// *************************
						// $sessionId = session_regenerate_id();
						// session_id($sessionId); // new session ID.
						// session_name("session_name"); // Sets the session name to the one set above.
						session_start(); // Start the php session

						// set authentication variables to session.
						$_SESSION['loggedIn'] ="true";
						$_SESSION['email'] = $email;
						

						// redirect to check if email id is verified.
						header("Location: email/check_email_status.php");

					}else{
						echo "Token not inserted for remember me.";
						}

				}else{

					// Remember me is not checked. So only start session and 
					// set $_SESSION['loggedIn']=="true";

					// set SESSION.
						
					// *********************
					// SECUTIRY TASK TO COMPLETE...
					// *************************
					// $sessionId = session_regenerate_id();
					// session_id($sessionId); // new session ID.
					// session_name("session_name"); // Sets the session name to the one set above.
					session_start(); // Start the php session

					// set authentication variables to session.
					$_SESSION['loggedIn'] ="true";
					$_SESSION['email'] = $email;
					

					// redirect to check if email id is verified.
					header("Location: email/check_email_status.php");

				}// remember me not checked.

			}else{
				echo "Invalid login credential.";
			}

		}else{
			// if there is 0 or more than 1 email id retrieved, then no login.
			die ("<font color='red'>Invalid credential. Please try again.</font>");
		}


// insert temp
}

?>