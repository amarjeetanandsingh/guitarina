<?php

/**
*	to check if email is verified after login.
*	There can be only two STATUS of a user 
* 	corresponding their mail in mail_verification table.
*
*	1) email.STATUS = NOT_VERIFIED =>> User registered only and email id still not verified.
*	2) email.STATUS = VERIFIED =>> user registered, email id veriried.
*
*
**/
 
include_once '../db_connect.php';   // included to connect to database.
include_once('../escapeInput.php');
// require password_compt.php to implement password_hash() in php 5.4.45
require "../../lib/password_compt.php";

// needs SESSSION SECURITY SETUP *(*********************************)
session_start();
// needs SESSSION SECURITY SETUP *(*********************************)

$email="";
$status = "";
$keyHash_org="";
$status_timestamp="";

// if user logged in and session is valid.
if(	isset($_SESSION['loggedIn']) && 
	$_SESSION['loggedIn']=="true"){

	//if valid email in session.
	if(	isset($_SESSION['email']) && $_SESSION['email'] !=""){
		// write retieve email status from database.
		
		$email = $_SESSION['email'];
		$query = ("	SELECT status FROM mail_verification WHERE email='$email' ORDER BY idmail_verification DESC	LIMIT 1;");
				
		$result = mysqli_query($link , $query)
					or trigger_error(mysqli_error($link ));
		if(	mysqli_affected_rows($link )==1 &&
			$row = mysqli_fetch_assoc($result)){
			
			// fetched status of given email and set it to session.
			$_SESSION['status'] = $row['status'];
			

			// Now two branches depending upon account/email STATUS...
			// NOT_VERIFIED, VERIFIED.

			switch ($_SESSION['status']) {
				
				// if email id of user is still to be verified.
				case 'NOT_VERIFIED':
					
					echo '	Verify your mail id by clicking on the link sent on 
							your email id, within a  week.';
					break;

				// if user verified its mail but not done payment.
				case 'VERIFIED':
					
					echo 'Mail id verified';
					header("Location: ../../videos.php");
					break;

				default:
					echo "Invalid account status.";

					break;
			}

		}else{
			echo 'Cannot retrieve account status.';
		}

		
	}else{
		echo "Invalid Email id in session.";
	}

}else{
	// if this page is requeste from anywhere else without login, 
	// logout them and intern send to login page.

	header('Location: ../logout.php');
}


?>