<?php

/**
*	This module mails a random key to a member which will be used to 
*	verify email.
*
**/


// password_hash() support to php5.4.45 using password_compt.php
include '../../lib/password_compt.php'  ;

 $mail_verif_key="";

function mail_verif_key($email, $link ){

$mail_sent_status = false;
$mail_saved_status = false;

// 	create a random string $mail_verif_key.
	$mail_verif_key = bin2hex(openssl_random_pseudo_bytes(32));
// 	create hash of $mail_verif_key_hash
	$mail_verif_key_hash = password_hash($mail_verif_key, PASSWORD_DEFAULT);


//	insert the $mail_verif_key_hash to database.

	$query = ("	INSERT INTO mail_verification (email,verificationhash,status)	
				VALUES('$email','$mail_verif_key_hash','NOT_VERIFIED');");
			
	$result = mysqli_query($link , $query)
				or trigger_error(mysqli_error($link ));
	if(mysqli_affected_rows($link )==1){
		$mail_saved_status = true;

		//	if $mail_saved_status = true then,
		//	mail activation link with $mail_verif_key and $email to $email

		// composing mail to be sent	.
		$url = 		"http://www.guitarina.com/php/email/varify_mail.php?mail_verif_key=$mail_verif_key&email=$email";
		$subject = 	"Guitarina email verification";
		$message = 	"Thank you for registering on <b>guitarina.com<b><br>
					Please click on the link below or copy paste the link 
					in browser address bar to activate your account.<br>
					The link is valid for 48 hours only.<br><br>
					$url <br>
					<br>
					Thanks and regards
					guitarina team.";

		$mail_sent_status = mail($email, $subject, $message);

		if($mail_sent_status){
			echo 	'An actavation link is sent on your mail id. Click on the link 
					in the mail to activate your account before you can use it.';
		}else{
			echo 'Account verification mail is not sent.';
		}
	}else{
		echo 'Verification mail not saved to database.';
	}

}
?>