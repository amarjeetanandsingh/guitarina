<?php
/* function to extract filtered data from user input.*/
	
function escapeInput($link, $data){
	$data = trim($data);
	$data = mysqli_real_escape_string($link, $data);
	$data = strip_tags($data);
	return $data;
}
?>