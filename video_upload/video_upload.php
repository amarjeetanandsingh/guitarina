<?php
include_once('../php/db_connect.php');
?>

<! DOCTYPE HTML>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" name="viewport" 
		content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=no" />
	<title>Upload Video</title>
	
  <!-- JS/JQuery -->
  <script type="text/javascript" src="js/jquery-2.1.3.min.js"></script>
  <!-- Bootstrap JS  -->
  <script src = "js/bootstrap.min.js"></script>
  <script src = "js/bootstrap-multiselect.js" ></script>
  <script src = "js/bootstrap-multiselect-collapsible-groups.js" ></script>

  <!-- The jQuery UI widget factory, can be omitted if jQuery UI is already included -->
  <script src="js/vendor/jquery.ui.widget.js"></script>
  <!-- The Iframe Transport is required for browsers without support for XHR file uploads -->
  <script src="js/jquery.iframe-transport.js"></script>
  <!-- The basic File Upload plugin -->
  <script src="js/jquery.fileupload.js"></script>

  <!-- CSS to Bootstrap -->
  <link rel="stylesheet" href="css/bootstrap.min.css" type="text/css"/>
   
  <!-- CSS for multiselect playlist dropdown-->
  <link rel="stylesheet" href="css/bootstrap-multiselect.css" type="text/css"/>

  <!-- Generic page styles -->
  <link rel="stylesheet" href="css/style.css">

  <!-- CSS to style the file input field as button and adjust the Bootstrap progress bars -->
  <link rel="stylesheet" href="css/jquery.fileupload.css">
  <link rel="shortcut icon" href="../../images/title.png" type="image/png" />
  <link rel="icon" href="../../images/title.ico" />
  
<!-- Initialize the multiselect playlist plugin: -->
<script>
    $(document).ready(function() {
        $('#playlist').multiselect({
          nonSelectedText: '-- Select playlist -- ',
          enableCaseInsensitiveFiltering: true,
          enableFullValueFiltering: true,
          includeSelectAllOption: true,
          buttonWidth: '200px',
          maxHeight: 200
        });
    });
</script>

</head>
<body>
<div class="container">
  <div class="container-fluid title">
      <h2>Video Upload</h2>
  </div>

<!-- Section to display the server response.-->
<div id="playlist_reponse" class="alert " role="alert"></div>

<form id="fileupload" action="server/php" method="post" enctype="multipart/form-data"
  class="form-horizontal">
  
  <!-- Title -->
  <div class="form-group">
    <label for="title" class="col-sm-2 control-label">Title</label>
    <div class="col-sm-6">
      <input type="text" class="form-control" name="title[]" id="title" 
      placeholder="Title..." value= <?php echo isset($_GET['title'])?("'".$_GET['title']."'"):("''"); ?> required>
    </div>
  </div>
  
  <!-- Video Status {publish, Unpublish}-->
  <div class="form-group">
    <label for="status" class="col-sm-2 control-label">Set video status</label>
    <div class="col-sm-6">
      <select class="form-control" name="status" id="status" >
        <!-- check if the url has $_get['status'] set and select as per the $_get param-->
        <option value="published" 
                <?php echo isset($_GET['status']) && $_GET['status']=='published'?
                ("selected='selected'"):("'none'"); ?> 
                >Published</option>
        <!-- check if the url has $_get['status'] set and select as per the $_get param-->
        <option value="unpublished" 
                <?php echo isset($_GET['status']) && $_GET['status']=='unpublished'?
                ("selected='selected'"):("'none'"); ?> 
                >UnPublished</option>
      </select>
    </div>
  </div>

  <div class="form-group">
    <label for="description" class="col-sm-2 control-label">Description</label>
    <div class="col-sm-6">
      <textarea class="form-control" rows="3" id="description" name="description[]" 
        placeholder="Video description..." required><?php echo isset($_GET['description'])?
        ($_GET['description']):(""); ?></textarea>      
    </div>
  </div>
  
  <!-- Playlist multiselect -->
  <div class="form-group">
    <label class="col-sm-2 control-label">Playlist</label>
    <div class="col-sm-6">
      <select id="playlist"  name="playlist[]" multiple="multiple">
        <?php 
          if (!$link ->connect_errno) {
            // if connected to database
            $query = "SELECT `name` FROM `playlist`;";
            $result = mysqli_query($link , $query);
            if($result && mysqli_num_rows($result) > 0){      
              while ($row = mysqli_fetch_array($result)) {
                $name = $row['name'];
                echo "<option value='".$name."'>".$name."</option>";                      
              }
            }
          }
        ?>
      </select>
       - OR - Create New Playlist -
      <!-- If playlist not available, click this to open a model to insert new playlist in db-->
      <button type="button" class="btn btn-success btn-md" data-toggle="modal" data-target="#addPlaylistModal">
        <span class="glyphicon glyphicon-plus-sign" aria-hidden="true"></span>
      </button>
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-2 control-label">Select video file</label>
    <div class="col-sm-6 ">
      <!-- The fileinput-button span is used to style the file input field as button -->
      <span class="btn btn-success fileinput-button">
          <i class="glyphicon glyphicon-plus"></i>
          <span>Select file</span>
          <!-- The file input field used as target for the file upload widget -->
          <input type="file" name="files[]" accept="video/mp4" required >
      </span>
    </div>
  </div>
  <!-- Progress bar-->
  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-6">
      <!-- The global progress bar -->
    <div id="progress" class="progress" >
        <div class="progress-bar progress-bar-success"></div> 
    </div>
    </div>
  </div>

  <div class="form-group">
    <div class="col-sm-offset-2 col-sm-10">
      <button id='buttonpublish' type="submit" class="btn btn-success btn-sub">Upload</button>
    </div>
  </div>
  
  <!-- Uploaded file size label-->
  <div class="form-group">
    <label id="lbluploaded_l" class="col-sm-2 control-label"></label>
    <div class="col-sm-10">
      <label id="lbluploaded_r" class="control-label" name="lbluploaded"></label><br/>
    </div>
  </div>

   <!-- Upload rate label-->
  <div class="form-group">
    <label id="lblbitrate_l" class="col-sm-2 control-label"></label>
    <div class="col-sm-10">
      <label id="lblbitrate_r" class="control-label"name="lblbitrate"></label><br/>
    </div>
  </div>

 <!-- uploaded time left label-->
  <div class="form-group">
    <label id="lbltimeleft_l" class="col-sm-2 control-label"></label>
    <div class="col-sm-10">
      <label id="lbltimeleft_r" class="control-label" name="lbltimeleft"></label>
    </div>
  </div>  
    
</form>
</div>

<!-- Modal to add new Playlist to database-->
<div class="modal fade" id="addPlaylistModal" tabindex="-1" role="dialog" 
aria-labelledby="addPlaylistModal" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-body">
        <br>
        <form id="addPlaylist" onsubmit="addPlaylist()" method="post" class="form-horizontal">
          <!-- Playlist name-->
          <div class="form-group">
            <label for="playlistName" class="col-sm-4 control-label">Enter Playlist Name</label>
            <div class="col-sm-8">
              <input type="text" class="form-control" id="playlistName" name="playlistName" placeholder="new playlist name">
            </div>
          </div>
          <!-- Buttons to submit and to close the modal.-->
          <div class="form-group">
            <div class="col-sm-offset-4 col-sm-8">
              <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
              <button type="submit" class="btn btn-primary" >Create</button>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>


<!-- jquery to submit playlist -->
<script>
function addPlaylist() {
    var addPlaylistFormData = $('#addPlaylist').serialize();
    var newPlaylistName = addPlaylistFormData['playlistName'];
    alert(newPlaylistName);
    $.ajax({
        url: "addPlaylist.php",
        type: "post",
        data: addPlaylistFormData,
        success: function(data){
            alert("Playlist added.");
            $('#playlist').append(new Option(newPlaylistName, newPlaylistName, true, true));
        },
        error: function(data){
            alert("Error in adding playlist.")
        }
    });
}
</script>
<script>
var globalProgress =0;
$(function () {
    'use strict';
    // Change this to the location of your server-side upload handler:
    var url = 'server/php/';
    $('#fileupload').fileupload({
        url: url,
        dataType: 'json',
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            globalProgress = progress;
            $('#progress .progress-bar').css(
                'width',
                progress + '%'
            );
            var rate = parseInt(data.bitrate/(8*1024),10);
            $('#lbluploaded_l').text('Uploaded ');
            $('#lbluploaded_r').text(progress+' %');
            $('#lblbitrate_l').text('Rate ');
            $('#lblbitrate_r').text((rate >= 1024 ? (parseInt(rate/1024,10) +" MBps" ) : (rate + " KBps") ));
            $('#lbltimeleft_l').text('Time Left ');
            $('#lbltimeleft_r').text(parseInt((data.total-data.loaded) * 8 / data.bitrate, 10)+" s");
        }
    }).prop('disabled', !$.support.fileInput)
        .parent().addClass($.support.fileInput ? undefined : 'disabled');
});
</script>

<script type="text/javascript">
    $(function () {
    $('#fileupload').fileupload({
        dataType: 'json',
        add: function (e, data) {            
            $('#buttonpublish').text('Upload')
                .click(function () {
                    data.context = $('<p/>').text('Uploading...').replaceAll($(this));
                    data.submit();
                });
        },
        done: function (e, data) {
            data.context.text('Upload finished.');
            
            // submitting data to addPlaylist.php page.
            var sendData = $("form").serialize();
            var videoId = data.result.files[0].name.split("::")[0] ;
            var videoStatus =  $('#status option:selected').val();
            var videoPlaylist = $('#playlist').val();
            sendData = ({
                        'videoId': videoId,
                        'status': videoStatus,
                        'playlist[]':videoPlaylist
                      });
           // insert data into video2playlist table to map the videos to playlist.
            $.ajax({
                url: "video2playlistMap.php",
                type: "post",
                data: sendData,
                success: function(data){
                    alert("Data added to playlist page. Php response: "+data);
                    $('#playlist_reponse').addClass("alert-success");
                    $('#playlist_reponse').html(data);
                },
                error: function(data){
                    alert("Error in adding data to playlist.: "+data);
                    $('#playlist_reponse').addClass("alert-danger");
                    $('#playlist_reponse').html(data);
                }
            });

            // update the status of video as published/unpublished.
        }
    });
});
</script>
</body>
</html>