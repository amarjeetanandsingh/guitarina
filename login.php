<?php
// to auto login if cookie is valid.

include_once 'php/db_connect.php';   // included to connect to database.
include_once ("php/escapeInput.php");
// password_hash support to php5.4.45
require 'lib/password_compt.php';


    // declearing variables to hold user input.
	$email = "";
	$token="";
	$auth_token_id="";
	$msg = "";
	$isCookieValid = true;
	$isSessionValid = true;


	// starting a session.
	session_start();


if (!$link ->connect_errno) {
// if connected to database, check for the cookie based login.....    
	// check if cookie is set, copy the value of cookie to local variables.
	isset($_COOKIE['token']) ? $token=$_COOKIE['token'] : $token="";
	isset($_COOKIE['email']) ? $email=$_COOKIE['email'] : $email="";
	isset($_COOKIE['id']) ? $auth_token_id=$_COOKIE['id'] : $auth_token_id="";

	
	// 1. ****************************
	// Firstly checking login through cookie. 

	// if cookie is set and not empty.... proceed for cookie validation.
	if(	isset($_COOKIE['token']) && $_COOKIE['token'] !="" &&
		isset($_COOKIE['email']) && $_COOKIE['email'] !="" &&
		isset($_COOKIE['id']) && $_COOKIE['id'] !="" ){

		// if cookie is set......

		$query = "	SELECT 	token,id FROM auth_token 
					WHERE 	email = '$email' AND 
							id = '$auth_token_id' AND
							status='unused'";
		$result = mysqli_query($link , $query)
					or die(mysqli_error($link ));
		if(!$result){
			$msg = "error in extracting cookie from database.";
			$isCookieValid = false;

		}else if(mysqli_num_rows($result)==1){
			// means cookie retrieved successfully.

			$row = mysqli_fetch_array($result);
			$token_hash_org = $row['token'];
			// $token = password_hash("$token", PASSWORD_DEFAULT);
			

			// if hash is equal or not.
			if(	$auth_token_id == $row['id'] && 
				password_verify($token, $token_hash_org) ){
				// if cookie is valid and AUTO-LOGIN successfull REDIRECT to user.php page. 

				// once login successful based on cookie... 
				// create new token and set to cookie and set to database.

				$token = uniqid(mt_rand());

				// create password_hash("", BCrypt) hash of $token to be stored in database.
				$token_hash = password_hash($token, PASSWORD_DEFAULT);

				// update the status of token as USED.
				$query = "UPDATE auth_token SET status='used' WHERE id=$auth_token_id;";
				mysqli_query($link , $query)
							or trigger_error(mysqli_error($link ));

				// insert new token in db.
				$query = "INSERT INTO auth_token(token,email,status)
								VALUES('$token_hash','$email','unused');";
				$result = mysqli_query($link , $query)
							or trigger_error(mysqli_error($link ));

				if(mysqli_affected_rows($link )==1){
				// if row is inserted successfully... 
					
					// get auto generated key.
					$auth_token_id = mysqli_insert_id($link );
					
					// SET COOKIES.
					setcookie('token', $token, time()+60,"/");
					setcookie('email',$email,time() +60,"/");
					setcookie('id',$auth_token_id, time()+60,"/");
					
					// set SESSION.

					// $sessionId = session_regenerate_id();
					// session_id($sessionId); // new session ID.
					// session_name("session_name"); // Sets the session name to the one set above.
					session_start(); // Start the php session

					// set authentication variables to session.
					$_SESSION['loggedIn'] ="true";
					$_SESSION['email'] = $email;

					// redirect to check if email id is verified.
					header("Location: php/email/check_email_status.php");

				}else{
					$msg = "error in inserting rows.";
					echo $msg;
				}
			}else if(	$auth_token_id == $row['id'] && 
						!(password_verify($token, $token_hash_org))){
				// if id is equal and $token is not equal => cookie is hacked. 
				$isCookieValid = false;

				// so DELETE FROM auth_token WHERE email = $email.
				$query = "DELETE FROM auth_token WHERE email = '$email';";
				if (!mysqli_query($link , $query)) 
				    $msg .= "Error deleting record: " . mysqli_error($link );
				echo $msg;

			}else{
				$isCookieValid = false;
				$msg = $msg.' Token is not equal';
			}
			echo "$msg";
		}else{
			$isCookieValid = false;
			$msg = $msg. " No result for this cookie is found in database.";
			echo $msg;
		}
		
	}else if(	isset($_SESSION['loggedIn']) && 
				$_SESSION['loggedIn'] == "true"){ // if no cookie but session is set.
// 2. ****************************
// Second checking login through Session. 


		// redirect to check if email id is verified.
		header("Location: php/email/check_email_status.php");

	}else{ // if no cookie and no session is valid.
		$isCookieValid = false;
		$isSessionValid = false;
		$msg= $msg.' session not set.';
	}

}

 if(!$isCookieValid && !$isSessionValid) {
 
	// Display login form if there is connection error.

 include 'php/header1.php';
?>
<br>
			<div class="col-md-12 some-notes">	
				<div class="container">
					<div class="title text-center">
						<h2>Login &nbsp; Please</h2>
						<br>
						<hr class="login-hr" width="300px" height="3px">
					</div>

		<!--  Login form starts here************************* -->
		<form class="form-horizontal" action="php/login.php"method="post">


			<!-- Email *******************************************-->

			<div class="form-group">
				<label class="col-sm-2 control-label" for="email">Email :</label>
				<div class="col-sm-10">
					<input class="form-control" type="email" name="email" id="email"
						required /><br>
				</div>
			</div>

			<!-- Password *******************************************-->
			<div class="form-group">
				<label class="col-sm-2 control-label" for="password">Password:</label>
				<div class="col-sm-10">
					<input class="form-control" type="password" name="password"
						id="password" required /><br>
				</div>
			</div>


			<!-- reCaptcha Group *******************************************-->
			<div class="form-group">
				<div class="col-sm-2 col-sm-offset-2">
					<div class="g-recaptcha"
						data-sitekey="6LcGIAwTAAAAACNWgXRWL7CEMolp6ajhX1TehhkP"></div>
				</div>
			</div>

			<!-- >Remember Me *******************************************-->
			<div class="form-group">
				<div class=" checkboxs col-sm-offset-2">
					<label> <input type="checkbox" name="rememberme"
						id="rememberme" value="rememberme"> Remember Me
					</label>
				</div>
			</div>

			<!-- Submit *******************************************-->
			<div class="form-group">
				<label class="col-sm-2 control-label" for="submit"></label>
				<div class="col-sm-10">
					<input class="btn btn-primary btn-sub" type="submit" value="Login" /> &nbsp;
					
					<a role="button" class="btn btn-default btn-sub" href="signup.php">Sign
						Up</a>
				</div>
			</div>
		</form>
		</div>
		</div>
	</div>		
</div>
	<?php
	include 'php/footer.php';
	?>

	</body>
</html>

<!-- Display login form if not connected to database. -->
<?php 
}
?>