CREATE DATABASE  IF NOT EXISTS `guitarina_db` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `guitarina_db`;
-- MySQL dump 10.13  Distrib 5.6.11, for Win32 (x86)
--
-- Host: localhost    Database: guitarina_db
-- ------------------------------------------------------
-- Server version	5.5.33

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_token`
--

DROP TABLE IF EXISTS `auth_token`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_token` (
  `email` varchar(100) DEFAULT NULL,
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` char(100) DEFAULT NULL,
  `status` char(10) DEFAULT NULL,
  `time_created` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  KEY `email` (`email`),
  CONSTRAINT `auth_token_email` FOREIGN KEY (`email`) REFERENCES `members` (`email`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_token`
--

LOCK TABLES `auth_token` WRITE;
/*!40000 ALTER TABLE `auth_token` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_token` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `mail_verification`
--

DROP TABLE IF EXISTS `mail_verification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `mail_verification` (
  `email` varchar(100) NOT NULL,
  `idmail_verification` int(11) NOT NULL AUTO_INCREMENT,
  `verificationhash` varchar(100) DEFAULT NULL,
  `status` enum('NOT_VERIFIED','VERIFIED') NOT NULL COMMENT 'can have only four values.\n1) "NOT_VERIFIED" = For those who just registered and their email is yet to be verified.\n2) "VERIFIED" = those who have verified their email id.',
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`idmail_verification`),
  KEY `email` (`email`),
  CONSTRAINT `mail_verification_ibfk_1` FOREIGN KEY (`email`) REFERENCES `members` (`email`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `mail_verification`
--

LOCK TABLES `mail_verification` WRITE;
/*!40000 ALTER TABLE `mail_verification` DISABLE KEYS */;
INSERT INTO `mail_verification` VALUES ('singhabhijeetanand@gmail.com',3,'1234','NOT_VERIFIED','2015-11-13 07:09:28'),('sanjeetjmp@gmail.com',14,'$2y$10$1JUvjYXhBXTlvxkiV5SFyuEqctD1EImwHN6j5VuC08oa8uJvFjsKy','NOT_VERIFIED','2015-11-13 08:30:12');
/*!40000 ALTER TABLE `mail_verification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `members`
--

DROP TABLE IF EXISTS `members`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `members` (
  `email` varchar(100) NOT NULL,
  `memberid` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `father` varchar(100) NOT NULL,
  `dob` varchar(15) NOT NULL,
  `password` varchar(400) NOT NULL,
  `phone` varchar(15) DEFAULT NULL,
  `doj` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`memberid`),
  KEY `email` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `members`
--

LOCK TABLES `members` WRITE;
/*!40000 ALTER TABLE `members` DISABLE KEYS */;
INSERT INTO `members` VALUES ('sarvmardan@gmail.com',16,'Amarjeet Anand','Anand kumar singh','1994/08/08','$2y$10$WUuQX1xQmb1yOLBsw3ynieNbsH3ANqPoKBF4IByV7mDlyLPVmLPFC','8797312939','2015-11-08 11:14:19'),('amarjeetanandsingh@gmail.com',20,'Amarjeet Anand','Anand Kumar Singh','1994/08/08','$2y$10$69wGe6yFlL829GSBEo5wTOszrYsTp/OkD3/37rcebvm7H5KxDkeVW','8797312939','2015-11-11 16:46:13'),('singhabhijeetanand@gmail.com',23,'Abhijeet Anand','Anand Kumar Singh','1994/08/08','$2y$10$SQc0AAFFaowSOlGTCPbdvepbTzeIbUIXop8D3IdahVA9uD9P/SWSq','8797312939','2015-11-12 07:20:09'),('sanjeetjmp@gmail.com',34,'Sanjeet kumar','Amit kumar chaurasiya','1994/08/08','$2y$10$Kyyu.0rXs3SZAC9Zv.42mOwXPO1Ui2uIX/659Z3ufxHGNROso9Lr6','8797312939','2015-11-13 08:30:12');
/*!40000 ALTER TABLE `members` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-11-13 15:40:32
