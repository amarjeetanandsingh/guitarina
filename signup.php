<?php
include 'php/header1.php';
?>

<!-- google js file included for reCaptcha v2-->
<script src='https://www.google.com/recaptcha/api.js'></script>


<br>
<div class="col-md-12 some-notes">	
	<div class="title">
		<center><h2> New Registration</h2></center>
		<hr color=black>

		<form class="form-horizontal col-sm-8 " name="registration" 
		method="POST" action="php/registration/register.php">


		<!-- Name Group *******************************************-->
		<div class="form-group">
			<label for="name"
				class="col-lg-2 col-md-2 col-sm-2 control-label">Name : </label>
			<div class="col-lg-10 col-md-5 col-sm-10">
				<input type="text" class="form-control" name="name" 
					id="name" placeholder="Enter your Name" />

			</div>
			
		</div>
		
		<!-- Father Name Group *******************************************-->
		<div class="form-group">
			<label for="fathername"
				class="col-lg-2 col-md-2 col-sm-2 control-label">Father: </label>
			<div class="col-lg-10 col-md-10 col-sm-10">
				<input type="text" class="form-control" name="fathername"
					id="fathername" placeholder="Father Name"  />

			</div>
		</div>

		<!-- DOB Group *******************************************-->
		<div class="form-group">
			<label for="dob" class="col-sm-2 control-label">Date of
				Birth : </label>
			<div class="col-sm-10">
				<input type="text" class="form-control" name="dob" id="dob"
					placeholder="Date of Birth" /> <span id="dobHelpBlock"
					class="help-block">Enter in <font color="#8AB839"><b>YYYY/MM/DD</b></font>
					format.
				</span>
			</div>
		</div>

		<!-- Intrested In *******************************************-->
		<div class="form-group">
			<label for="branch" class="col-sm-2 control-label">Tutorial :
			</label>
			<div class="col-sm-10">
				<select name="tutorial" class="form-control" >
					<option value="">-- Select Tutorial --</option>
					<option value="guitar">Guitar</option>
					<option value="synth">Synthesizer</option>
					<option value="vocal">Vocal</option>
					<option value="violin">Violin</option>
				</select>
			</div>
		</div>

		<!-- Email Group *******************************************-->
		<div class="form-group " id="emailGroup">
			<label for="email" class="col-sm-2 control-label">Email : </label>
			<div class="col-sm-10">
				<input type= "email" class="form-control" name="email" id="email"
					placeholder="Email ID" /><br>
				<span id="emailHelpBlock" class="help-block"></span>
			</div>
		</div>

		<!-- Phone Group *******************************************-->
		<div class="form-group">
			<label for="phone" class="col-sm-2 control-label">Phone : </label>
			<div class="col-sm-10">
				<input type="text" class="form-control" name="phone" id="phone" 
				pattern="^[0-9]{10}$" placeholder="Phone No." /> 
				<span id="phoneHelpBlock" class="help-block"> Enter 10 digit mobile number.</span>
			</div>
		</div>

		<!-- Password Group *******************************************-->
		<div class="form-group">
			<label for="password" class="col-sm-2 control-label">Password
				: </label>
			<div class="col-sm-10">
				<input type="password" class="form-control" name="password"
					id="password" />
			</div>
		</div>

		<div class="form-group">
			<label for="confirmPassword" class="col-sm-2 control-label">Confirm
				Password : </label>
			<div class="col-sm-10">
				<input type="password" class="form-control" name="confirmPassword"
					id="confirmPassword"  />
			</div>
		</div>

		<!-- reCaptcha Group *******************************************-->
		<div class="form-group " id="reCaptchaGroup">
			<label class="col-sm-2 control-label"></label> 
			<div class="col-sm-10 col-sm-offset-2">
				<div class="g-recaptcha" data-sitekey="6LcyWA4TAAAAADngB0DqjsD2ujmVbQ1sQ3-AkBMX"></div>
			</div>
		</div>

		<!-- Submit Group *******************************************-->
		<div class="form-group">
			<label class="col-sm-2 control-label"></label>
			<div class="col-sm-10">
				<input type="submit" class="btn btn-default btn-sm btneff" name="submit"	
					value="Submit" id="submit"  />
			</div>
		</div>
		</form>
	</div>
</div>



<div class="container-fluid notes">
  	<div class="row">
   		<div class="col-md-12 col-sm-12 col-xs-12 notes-bg">
    		<div class="container">
    			<div class="col-md-8 col-sm-8 col-xs-12">
     				<p>I believe every guitar player inherently has something unique about their playing. They just have to identify what makes them different and develop it.<br>
     				I mean, give me a guitar, give me a piano, give me a broom and string, I wouldn't get bored anywhere.
				 	</p>
    			</div>
    			<div class="col-md-offset-1 col-md-2 col-md-offset-1 col-sm-offset-1 col-sm-2 col-sm-offset-1 col-xs-12">
     				<button type="button" class="btn btn-default btneff">Read More</button>
    			</div>
   			</div>
   		</div>
  	</div> 
</div>



<?php 
include 'php/footer.php';
?>
