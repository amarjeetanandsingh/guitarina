<?php
include 'php/header1.php';
?>
<link rel="stylesheet" type="text/css" href="css/photoGallery.css">

<body>

	<br><br><br><br><br><br>
	<br>
	<div class="col-md-12 some-notes"> 
      <div class="title"> <h2>Some Of the Distinctive Moments</h2> 
      </div> 
		<br>
    
	<div id="photoGallery-container">
		<img class="photoGallery" data-src="photos/1.jpg" src="photos/1.jpg" data-id="pic_1" data-desc="Image Caption1">
		<img class="photoGallery" data-src="photos/2.jpg" src="photos/2.jpg" data-id="pic_2" data-desc="Image Caption2">
		<img class="photoGallery" data-src="photos/3.jpg" src="photos/3.jpg" data-id="pic_3" data-desc="Image Caption3">
		<img class="photoGallery" data-src="photos/4.jpg" src="photos/4.jpg" data-id="pic_4" data-desc="Image Caption4">
		<img class="photoGallery" data-src="photos/5.jpg" src="photos/5.jpg" data-id="pic_5" data-desc="Image Caption5">
		<img class="photoGallery" data-src="photos/6.jpg" src="photos/6.jpg" data-id="pic_6" data-desc="Image Caption6">
		<img class="photoGallery" data-src="photos/7.jpg" src="photos/7.jpg" data-id="pic_7" data-desc="Image Caption7">
		<img class="photoGallery" data-src="photos/8.jpg" src="photos/8.jpg" data-id="pic_8" data-desc="Image Caption8">
		<img class="photoGallery" data-src="photos/9.jpg" src="photos/9.jpg" data-id="pic_9" data-desc="Image Caption9">
		<img class="photoGallery" data-src="photos/10.jpg" src="photos/10.jpg" data-id="pic_10" data-desc="Image Caption10">
		<img class="photoGallery" data-src="photos/11.jpg" src="photos/11.jpg" data-id="pic_11" data-desc="Image Caption11">
		<img class="photoGallery" data-src="photos/12.jpg" src="photos/12.jpg" data-id="pic_12" data-desc="Image Caption12">
		<img class="photoGallery" data-src="photos/13.jpg" src="photos/13.jpg" data-id="pic_13" data-desc="Image Caption13">
		<img class="photoGallery" data-src="photos/14.jpg" src="photos/14.jpg" data-id="pic_14" data-desc="Image Caption14">
		<img class="photoGallery" data-src="photos/15.jpg" src="photos/15.jpg" data-id="pic_15" data-desc="Image Caption15">
		<img class="photoGallery" data-src="photos/16.jpg" src="photos/16.jpg" data-id="pic_16" data-desc="Image Caption16">
	
	</div>
		<?php
	include 'php/footer.php';
?>
<script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
<script type="text/javascript" src="js/photoGallery.js"></script>
<script type="text/javascript">
	$(function(){
		var photoGallery = new PhotoGallery();
	});
</script>
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36251023-1']);
  _gaq.push(['_setDomainName', 'jqueryscript.net']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>
</body>
</html>